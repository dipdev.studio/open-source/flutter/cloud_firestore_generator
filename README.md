A Cloud Firestore Generator package

For [Cloud Firestore Anotation][anotation] package

[anotation]: https://gitlab.com/dipdev.studio/open-source/flutter/colud_firestore_anotation

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/dipdev.studio/open-source/flutter/cloud_firestore_generator/-/issues