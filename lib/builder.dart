import 'package:build/build.dart';
import 'package:cloud_firestore_generator/src/cloud_firestore_generator.dart';
import 'package:source_gen/source_gen.dart';

Builder cloudFirestoreDocument(BuilderOptions options) =>
    SharedPartBuilder([CloudFirestoreGenerator()], 'todo_reporter');
