import 'dart:async';
import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:cloud_firestore_anotation/cloud_firestore_anotation.dart';
import 'package:source_gen/source_gen.dart';

class CloudFirestoreGenerator
    extends GeneratorForAnnotation<CloudFirestoreDocument> {
  @override
  FutureOr<String> generateForAnnotatedElement(
      Element element, ConstantReader annotation, BuildStep buildStep) {
    return '// Hey! Annotation found!';
  }
}
